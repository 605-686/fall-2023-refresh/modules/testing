package com.androidbyexample.compose.testing

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(version = 1, entities = [Movie::class])
abstract class MovieDatabase: RoomDatabase() {
    abstract val dao: MovieDAO
}